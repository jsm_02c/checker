class CheckerController < ApplicationController
    
    def show
    end
    
    def checklist_param
        params.require(:checklist).permit(
          checkcomp_attributes: [:id, :checkerlist_id, :checked, :check_detail, :_destroy]
        )  
    end
end
