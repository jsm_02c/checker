class Checklist < ActiveRecord::Base
    has_many :checkcomps, dependent: :destroy
    accepts_nested_attributes_for :checkcomps, allow_destroy: true
end
